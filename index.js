const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/', require('./routes/signUp_logIn'))
app.use('/', require('./routes/users'))
app.use('/', require('./routes/admin'))

app.get('/', (req, res) => {
    res.send("hello")
})

app.get('/signup', (req, res) => {
    res.sendFile(path.join(__dirname + '/view/signUp.html'));
})

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname + '/view/login.html'));
})

app.listen(3000, (err) => {
    if (!err) {
        console.log("server running on port 3000 ....");
    }
})
